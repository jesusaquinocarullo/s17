let start = 'Activity S17';
console.log(start);

function inputFullName(){
	let fullName = prompt('Enter your full name: ');
	let age = prompt('Enter your age: ');
	let location = prompt('Enter your location: ');
	let thanks = alert('Success! Thank you!')

	console.log('Hello, ' + fullName);
	console.log('You are ' + age + ' years old.');
	console.log('You live in ' + location);	
};

inputFullName();

let topBandArtists = 'Top 5 Favorite Bands / Musical Artists';
console.log(topBandArtists);

function printFavoriteBandsMusicArtists(){
	console.log('1. My Chemical Romance');
	console.log('2. Fall Out Boy');
	console.log('3. All Time Low');
	console.log('4. Green Day');
	console.log('5. Boys Like Girls');
};

printFavoriteBandsMusicArtists();

let topMovies = 'Top 5 Favorite Movies of All Time';
console.log(topMovies);

let movieRating = 'Rotten Tomatoes Rating: ';
function printFavoriteMovies(){
	console.log('1. The Conjuring');
	let firstMovieRating = '86%';
	console.log(movieRating + firstMovieRating);
	console.log('2. The Conjuring 2');
	let secondMovieRating = '80%';
	console.log(movieRating + secondMovieRating);
	console.log('3. The Conjuring: The Devil Made Me Do It');
	let thirdMovieRating = '56%';
	console.log(movieRating + thirdMovieRating);
	console.log('4. The Nun');
	let fourthMovieRating = '25%';
	console.log(movieRating + fourthMovieRating);
	console.log('5. Annabelle');
	let fifthMovieRating = '29%';
	console.log(movieRating + fifthMovieRating);
};

printFavoriteMovies();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

